#!/bin/bash
#$ -cwd
#$ -q seal
#$ -j y

find . -maxdepth 1 -name "fapi_extract_job_array_pvd.sh.o*" -exec grep -L " has run for " {} \; > failed_fapi_extract_jobs.txt
