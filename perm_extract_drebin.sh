#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -pe openmp 2
#$ -j y
#$ -t 1-5560
#$ -r y
#$ -ckpt restart
#module load enthought_python/7.3.2
module purge
module load Cluster_Defaults
#module load anaconda/2-2.3.0
module load java/1.8.0.51
module list
SEEDFILE=drebin_apk_path_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
time scripts/perm_extraction.sh $SEED
