#!/bin/bash
#$ -cwd
#$ -pe openmp 4
#$ -q seal,free*,pub*
#$ -j y
#$ -t 1-5560
#$ -r y

module load anaconda/2.7-4.3.1
SEEDFILE=drebin_apk_path_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/enec.sh $SEED
