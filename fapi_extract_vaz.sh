#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -pe openmp 4
#$ -j y
#$ -t 1-24999
#$ -r y
#module load enthought_python/7.3.2
module purge
module load Cluster_Defaults
#module load anaconda/2-2.3.0
module load java/1.8.0.51
module list
SEEDFILE=/share/seal/joshug4/workspace/android-reflection-analysis/verified_androzoo_benign_apps.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
time scripts/papi_extraction.sh $SEED
