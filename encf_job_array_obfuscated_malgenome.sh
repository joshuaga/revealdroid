#!/bin/bash
#$ -cwd
#$ -pe openmp 4
#$ -q seal,free64,pub64
#$ -j y
#$ -t 1-1207
#$ -r y
module load enthought_python/7.3.2
SEEDFILE=obfuscated_malgenome_app_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/extract_native_code_files.py $SEED
