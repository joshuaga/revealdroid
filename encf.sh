#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -j y

module load enthought_python/7.3.2

./extract_native_code_files.py $1