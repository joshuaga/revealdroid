#!/usr/bin/env python

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.datasets import load_iris
from sklearn.feature_selection import SelectFromModel

import numpy as np
import h5py
import argparse
import time
import os
import sys

parser = argparse.ArgumentParser(description='script for conducting feature selection on hdf5 file')

parser.add_argument('-f',help='HDF5 input file',required=True)
args = parser.parse_args()

start_time=time.time()
h5f = h5py.File(args.f,'r')
X=h5f['X'][:]
y=h5f['y'][:]
apk_names=h5f['apk_names'][:]
fnames=h5f['fnames'][:]
print 'time to load {}: {}'.format(args.f,time.time()-start_time)
sys.stdout.flush()

print 'fnames type: {}'.format(type(fnames))
#print 'fnames: {}'.format([n.decode() for n in fnames])

print 'X.shape: {}'.format(X.shape)
print 'y.shape: {}'.format(y.shape)
#print 'apk_names: {}'.format(apk_names)
sys.stdout.flush()

start_time=time.time()
clf = ExtraTreesClassifier()
clf = clf.fit(X, y)
print clf.feature_importances_  
model = SelectFromModel(clf, prefit=True)
X_new = model.transform(X)
print 'X_new.shape: {}'.format(X_new.shape)
sys.stdout.flush()

sup=model.get_support()
sel_features=[n.encode('utf8')  for i,n in enumerate(fnames) if sup[i] ]
print len(sel_features)
assert len(sel_features) == X_new.shape[1]
print 'feature selection time: {}'.format(time.time()-start_time)
sys.stdout.flush()

start_time=time.time()
in_basename = os.path.splitext(args.f)[0]
reduced_filename=in_basename + '_reduced.hdf'
new_f = h5py.File(reduced_filename,'w')
new_f.create_dataset('X',data=X_new,dtype=int)
dt = h5py.special_dtype(vlen=unicode)
new_f.create_dataset('y',data=np.array(y),dtype=dt)
new_f.create_dataset('apk_names',data=np.array(apk_names),dtype=dt)
new_f.create_dataset('fnames',data=np.array(sel_features),dtype=dt)
new_f.close()
print 'time to store reduced dataset: {}'.format(time.time()-start_time)
sys.stdout.flush()

