# README #

RevealDroid is a machine-learning based approach for detecting malicious Android apps and identifying their families that provides a selectable set of features for achieving different trade-offs between obfuscation resiliency, efficiency of analysis, and accuracy. 

For the latest version of RevealDroid, you will also need to use our [android-reflection-analysis](https://bitbucket.org/joshuaga/android-reflection-analysis) project, which contains reflection features and some sci-kit learn-based functionality.

Notice that our feature extraction below now includes native code extraction.

To set up the dataset for RevealDroid from previous experiments, please follow the instructions [here](http://seal.ics.uci.edu/projects/revealdroid/index.html).

## Dependencies and Building ##

For native code features, you will need to have the Android NDK on your system and its directory set to the PATH environment variable so that RevealDroid can use ```ndk-which```.

handleflowdroid - data structures for handling FlowDroid output for RevealDroid - https://bitbucket.org/joshuaga/handleflowdroid

seal-utils - has some library classes you'll need (e.g., PermMapper). Please use the **phenomenon** branch - https://bitbucket.org/joshuaga/seal-utils/overview

* Use the master branch

FlowDroid - a modified version of FlowDroid that writes the output that RevealDroid needs and accepts input from RevealDroid - https://bitbucket.org/joshuaga/soot-infoflow-android

* Use the develop branch

soot-infoflow and its dependencies - obtain the forked versions of heros, jasmin, soot, soot-infoflow, and seal-utils located here - https://github.com/jgarci40

* Use the selected branch of soot-infoflow
* Use the develop branch of heros
* Use the develop branch of soot
* Use the develop branch of jasmin
* Use the master branch of seal-utils

**NOTE**: Weka is no longer needed to run RevealDroid; however, if you want to use some of its legacy code, you can follow the information on the line below.
For weka, you need to use the package manager for weka 3.7.x and install the simpleEducationalLearningSchemes package.

If you place the directories containing the above projects at the same directory level as revealdroid, you can use 'ant -f build_argo.xml' without the quotes, to build revealdroid.

We also recommend you use JDK 8 with RevealDroid.

To build the Java portion of RevealDroid, you can use the following ant configuration files:

```
revealdroid.xml - ant build file
revealdroid.properties - ant properties file
```

For example, you can run the following command in the ```revealdroid/``` directory to build the Java portion of RevealDroid:

```ant -f revealdroid.xml -propertyfile revealdroid.properties```

Also, you'll need to change the ```jdk.home.1.80.0_102``` property in ```revealdroid.properties``` to point to the correct directory of the JDK you wish to use.

The python package dependencies include the following:

* scikit-learn 0.18.* and above - available [here](http://scikit-learn.org/stable/index.html)
* python-magic 0.4.6 - available [here](https://pypi.python.org/pypi/python-magic/0.4.6) or [with conda](https://anaconda.org/birdhouse/python-magic)

## Feature Extraction ##

The following classes can be used to extract individual features for use in RevealDroid:


* revealdroid.features.apiusage.ExtractCategorizedApiCount - for sensitive API features
* revealdroid.features.apiusage.ExtractApiUsageFeatures - for package API features
* revealdroid.features.intent.ExtractSystemIntentActions - for Intent action features

The slightly modified version of FlowDroid mentioned above can be used to extract flows from individual APKs to obtain their flows as features in Weka format that can subsequently be utilized by RevealDroid. 


To extract features in batch (i.e., for multiple APKs in a directory), you can use the following classes:

* revealdroid.features.intent.BatchExtractSystemIntentActions - for Intent action features
* revealdroid.features.apiusage.BatchCategorizedApiCount - for sensitive API features
* revealdroid.features.apiusage.BatchExtractApiUsageFeatures - for package API features
* revealDroid.Test2 - for flow features

For native code features, refer to ```extract_native_external_calls.py```.


For batch extraction, you must provide as input a directory where each sub-directory corresponds to a label utilized by RevealDroid (i.e., benign, malware, or malware family). Each sub-directory contains corresponding APKs.


You may refer to the descriptions of each input parameter of the batch feature extraction classes for more details.

To extract native code, use the following scripts:

* encf.sh - to disassemble an apk
* eecfd.sh - to extract external calls from the dissassembly

For package API extraction, you can also simply use ```scripts/papi_extraction.sh```.

## Classification ##

**Please note that the use of Weka is deprecated. Please use sklearn for machine learning and the datasets stored in sklearn-friendly formats.**

RevealDroid gives you multiple classes for training and testing using Weka ARFF files, which are produced by RevealDroid:

* revealdroid.classify.CrossValidate - performs cross-validation over a Weka ARFF file
* revealdroid.classify.TrainTest - performs training on one ARFF file and testing on another
* revealdroid.classify.TrainTestDifference - performs training on the difference between an input ARFF file and another ARFF file for testing, and tests on the latter file
* revealdroid.classify.TrainTestOnSelection - performs training and testing using hashes selected from one file for training and another for testing
* revealdroid.classify.TrainTestSelectFileNames - performs training and testing using apk names selected from one file for training and another for testing

You can check the outputted help descriptions of each executable class to learn more about input parameters.

## ARFF Files ##

**Please note that the use of ARFF files are deprecated. Please use sklearn for machine learning and the datasets stored in sklearn-friendly formats.**

Many ARFF files with already-extracted features exist for RevealDroid. The general format of the ARFF file names are as ```<source apks>_<features>_<other>.arff```.

Here are the possible source names:

* benign - Benign APKs
* malgenome - The original Malware Genome APKs
* virusshare - VirusShare APKs
* expanded _malgenome - APKs from families belonging to the original Malware Genome, but not including apps from the original Malware Genome
* combined _malgenome - includes the original Malware Genome APKs and the expanded_malgenome APKs
* obfuscated_malgenome - Malware Genome apps transformed using DroidChameleon

Here are the possible features:
* catapicount - sensitive API features
* intent actions - Intent action features
* apiusage_actions - package API features
* flows or flowdroid - flow features

Here are some of the other names that may be included in an ARFF file name:
* 2way - converted from family labels to 2-way benign or malicious labels

## Scripts ##

Example scripts (*.sh files) exist that can be altered to run different parts of RevealDroid from the command line. The ARFF file are already present so many of the existing scripts should work, but you need to modify the classpath or environment variables in the scripts.

The naming of scripts are in the following format ```<revealdroid class run>_<apk dataset>_<features>_<possible classifier>.sh```:

Here is an abbreviation guide for some of the <revealdroid class run> names:
* tt - TrainTest
* tts - TrainTestOnSelection
* ttsfn - TrainTestSelectFileNames

Here is an abbreviation guide for some of the datasets, which follow a similar naming to the source apks in the ARFF Files section above:
* bgv - includes benign apps, the original Malware Genome, and VirusShare apps

For features:
* sapi - sensitive API features
* papi - package API features
* intent - Intent action features
* flows - flow features


For classifiers:
* j48 - C4.5 decision tree classifier
* ib1 - 1NN classifier

Analyzing a single app example:

RevealDroid includes a script, ```excl_prn.sh```, that uses sensitive API features, reflection features, and native code features to obtain analysis results for a single apk file. The script takes a single argument, the location of the apk file. To utilize this script, you need to set the environment variable RD_WORKSPACE to a directory containing the revealdroid/ and android-reflection-analysis/ projects and all its dependency project (e.g., handleflowdroid, soot, jasmin, etc.). For example, if your directory structure is as follows:

```
├── /Users/joshua/rd_workspace
    ├── revealdroid
    ├── android-reflection-analysis
    ├── handleflowdroid
    ├── soot
    ├── soot-infoflow-android
    ├── soot-infoflow
    ├── heros
    ├── jasmin
    ├── seal-utils
```

Then, you need to set RD_WORKSPACE as follows before executing excl_prn.sh:
```
export RD_WORSPACE=rd_workspace
```

You can add that line to ```~/.bashrc```, ```~/.bash_profile```, or the appropriate configuration file for your shell and OS.