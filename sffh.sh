#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -j y

module load anaconda/2.7-4.3.1
./select_features_from_hdf.py $@
