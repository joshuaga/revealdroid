#!/bin/bash
#$ -cwd
#$ -q seal,pub*
#$ -j y
#$ -ckpt restart

find . -not -name "*.tar.gz" -name "encf*sh.o*" -or -name "submit_job_array_*encf.sh.o*" -exec grep -Hn "androideabi" {} \;
