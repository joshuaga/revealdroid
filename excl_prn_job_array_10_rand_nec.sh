#!/bin/bash
#$ -cwd
#$ -pe openmp 4
#$ -q seal,free*,pub*
#$ -j y
#$ -t 1-10
#$ -r y

module load enthought_python/7.3.2
SEEDFILE=10_random_nec_apps_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/excl_prn.sh  $SEED

