#!/bin/bash
#$ -cwd
#$ -pe openmp 1
#$ -q seal,free64,pub64
#$ -j y
#$ -t 1-1258
#$ -r y
#$ -ckpt restart
module load enthought_python/7.3.2
SEEDFILE=malgenome_disassembly_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/extract_external_calls_from_disassembly.py $SEED
