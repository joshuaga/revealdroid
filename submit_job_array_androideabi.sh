#!/bin/bash
#$ -cwd
#$ -pe openmp 1
#$ -q seal,free*,pub*
#$ -j y
#$ -t 1-10000
#$ -r y
#$ -ckpt restart
module load enthought_python/7.3.2
SEEDFILE=encf_list_androideabi.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/extract_external_calls_from_disassembly.py $SEED
