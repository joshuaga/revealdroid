#!/bin/bash
#$ -cwd
#$ -pe openmp 1
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/revealdroid_logs/
#$ -e /share/seal/joshug4/revealdroid_logs/
#$ -t 1-1740
#$ -ckpt restart

module load anaconda/2-2.3.0
SEEDFILE=../android-reflection-analysis/res/pcomp_no_obfs_benign_apps.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/extract_prn_features.sh  $SEED

