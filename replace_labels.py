#!/usr/bin/env python

import argparse
import h5py

def main():
    parser = argparse.ArgumentParser(description='replace all instances of a label with another label in an HDF5 file')
    parser.add_argument('--hdf',help='HDF5 file with labels you\'d like to replace')
    parser.add_argument('--old-label',help='label to be replaced')
    parser.add_argument('--new-label',help='label to use in place of old label')
    args = parser.parse_args()

    h5f = h5py.File(args.hdf)
    y = h5f['y'][:]
    for i,label in enumerate(y):
        if label == args.old_label:
            y[i]=args.new_label

    del h5f['y']
    dt = h5py.special_dtype(vlen=unicode)
    h5f.create_dataset('y',data=y,dtype=dt)
    h5f.close()

if __name__ == '__main__':
    main()

