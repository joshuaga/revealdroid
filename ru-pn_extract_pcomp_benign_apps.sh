#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -pe openmp 1
#$ -o /share/seal/joshug4/revealdroid_logs/
#$ -e /share/seal/joshug4/revealdroid_logs/
#$ -t 1-1742
#$ -ckpt restart
#module load enthought_python/7.3.2
module purge
module load Cluster_Defaults
#module load anaconda/2-2.3.0
module load java/1.8.0.51
module list
SEEDFILE=/share/seal/joshug4/workspace/android-reflection-analysis/pcomp_benign_no_obfs_paths.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
time scripts/perm_extraction.sh $SEED
time scripts/used_perm_extraction.sh $SEED
time scripts/netaddr_extraction.sh $SEED
