#!/bin/bash
#$ -cwd
#$ -q seal,free*,pub*
#$ -j y
#$ -t 1-10000
#$ -r y
#$ -ckpt restart
module load enthought_python/7.3.2
SEEDFILE=pvdm_disassembly_file_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/extract_external_calls_from_disassembly.py $SEED
