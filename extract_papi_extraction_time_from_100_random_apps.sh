#!/bin/bash

IFS=$'\n'
times=($(ls papi_extract_job_array_100_random_apps.sh.o4843529.* | xargs grep -P 'real\s+'))
r="m([\.0-9]+)s"
#printf "%s\n" "${times[@]}"
vals=()
for t in ${times[@]}
do
	#echo $t
	[[ $t =~ $r ]]
	tstr="${BASH_REMATCH[1]}"
	#echo "${tstr}"
	vals+=(${tstr})
done

printf "%s\n" "${vals[@]}"

sum=0
for i in "${vals[@]}"; do
	sum=$(echo $sum + $i | bc -l);
done
echo "sum: $sum"

len=${#vals[@]}
echo len: $len
avg=$( echo "$sum/$len" | bc -l ) 
echo "avg: " ${avg}
