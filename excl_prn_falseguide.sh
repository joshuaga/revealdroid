#!/bin/bash
#$ -cwd
#$ -pe openmp 4
#$ -q seal,free*,pub*
#$ -j y
#$ -t 1-44
#$ -r y

module load anaconda/2-2.3.0
SEEDFILE=falseguide_app_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/excl_prn.sh  $SEED

