#!/usr/bin/env python

import argparse
import zipfile
import os
import magic
import subprocess

parser = argparse.ArgumentParser(description='Extract native-code features from apk')
parser.add_argument('apk',help='apk file location')
args = parser.parse_args()
print args.apk

apk_f = zipfile.ZipFile(args.apk,"r")

basename = os.path.basename(args.apk)

DATA_NATIVE_FILES="data/native_files"
apk_native_dir=DATA_NATIVE_FILES + "/" + basename

try:
    objdump_command = subprocess.check_output(["ndk-which","objdump"])
    objdump_command = objdump_command.strip()
except subprocess.CalledProcessError as e:
    print "command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)

if not os.path.exists(apk_native_dir):
    os.makedirs(apk_native_dir)

for name in apk_f.namelist():
    file_info = magic.from_buffer(apk_f.read(name))
    if "ELF" in file_info:
        print "Extracting {}".format(file_info)
        apk_f.extract(name,apk_native_dir)
        print "Running {} -d on {}".format(objdump_command,name)
        try:
            print subprocess.check_output([objdump_command,"-d","-marm",apk_native_dir+"/"+name])
        except subprocess.CalledProcessError as e:
            print "command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)

print "Reached end of script..."
