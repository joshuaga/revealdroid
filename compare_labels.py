#!/usr/bin/env python

import argparse
import h5py

def create_apk_label_dict(apk_names,y):
    if len(apk_names) != len(y):
        raise ValueError('apk_names should be the same length as y')
    apk_label_dict=dict()
    for i,name in enumerate(apk_names):
        apk_label_dict[name] = y[i]
    return apk_label_dict
    
def print_discreps(apk_labels1,apk_labels2,f2):
    for apk_name,label in apk_labels1.items():
        if not apk_name in apk_labels2:
            print '{} is missing apk {}'.format(f2, apk_name)
            continue
        if apk_labels1[apk_name] != apk_labels2[apk_name]:
            print '{}: {}, {}'.format(apk_name,apk_labels1[apk_name],apk_labels2[apk_name])
        if apk_labels1[apk_name] == 'Benign' and apk_labels2[apk_name] != 'unknown':
            print 'Whoa if {} is Benign, the other file should be unknown'.format(apk_name)
        if apk_labels1[apk_name] == 'unknown' and apk_labels2[apk_name] != 'Benign':
            print 'Hmm if {} is unknown, the other file should probably have Benign'.format(apk_name)

def main():
    parser = argparse.ArgumentParser(description='compare labels across two HDF files')
    parser.add_argument('-f1',help='first HDF5 input file',required=True)
    parser.add_argument('-f2',help='second HDF5 input file',required=True)
    args = parser.parse_args()
    
    h5f1 = h5py.File(args.f1)
    h5f2 = h5py.File(args.f2)
    
    apk_names1 = h5f1['apk_names'][:]
    apk_names2 = h5f2['apk_names'][:]
    y1 = h5f1['y'][:]
    y2 = h5f2['y'][:]
        
    apk_labels1 = create_apk_label_dict(apk_names1,y1)
    apk_labels2 = create_apk_label_dict(apk_names2,y2)

    print 'Printing discrepancies by iterating over apk names in {} file:'.format(args.f1)
    print_discreps(apk_labels1,apk_labels2,args.f2)

    print 'Printing discrepancies by iterating over apk names in {} file:'.format(args.f2)
    print_discreps(apk_labels2,apk_labels1,args.f1)
    
if __name__ == '__main__':
    main()
