#!/bin/bash
#$ -cwd
#$ -q seal
#$ -j y

echo 'searching for full paths of APKs...'
cat pvd_papi_apps.txt | xargs -I{} find /share/seal/joshug4/playdrone_apps/ /share/seal/joshug4/virusshare/ /share/seal/joshug4/drebin/dataset/drebin-all/ /share/seal/joshug4/android/apps/Benign/ -name '{}' > pvd_absolute_paths_apps_list.txt

