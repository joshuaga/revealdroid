#!/usr/bin/env python

import json
import unicodedata
from pprint import pprint

with open('obfuscated_reflection_dataset.json') as f:
    data = json.load(f)
    
pprint(data)

def convert_unicode_to_string(uni):
    return unicodedata.normalize('NFKD',uni).encode('ascii','ignore')

