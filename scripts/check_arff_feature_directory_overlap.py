#!/usr/bin/python

import argparse
from os import listdir
from os.path import isfile, join



parser = argparse.ArgumentParser()
parser.add_argument("--arff","-a",help="arff file to copmare instances against",required=True)
parser.add_argument("--dir","-d",help="directory with features files to compare against",required=True)
parser.add_argument("--prefix","-p",help="prefix of feature files",required=True)
parser.add_argument("--suffix","-s",help="suffix of feature files",required=True)
args = parser.parse_args()

#print args

onlyfiles = [ f for f in listdir(args.dir) if isfile(join(args.dir,f)) ]

apks = set()
for f in onlyfiles:
    if f.startswith(args.prefix):
        noprefix = str(f)[len(args.prefix):]        
        if noprefix.endswith(args.suffix):
            apk_filename = noprefix[:-len(args.suffix)] + ".apk"
            #print apk_filename
            apks.add(apk_filename)

a = open(args.arff,'r')
hitDataLine=False
for line in a:
    if "@data" in line:
        hitDataLine=True
    if hitDataLine:
        ridx = line.rfind('.apk')
        if not ridx  == -1:
            eidx = ridx + len('.apk')
            arff_apk = line[:eidx]
            if arff_apk in apks:
                print line,
            
            
        
