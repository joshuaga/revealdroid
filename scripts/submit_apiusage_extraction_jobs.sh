#!/bin/bash
#$ -cwd
#$ -pe openmp 1-4
#$ -q free64,pub64
#$ -m as
#$ -j y
#$ -ckpt blcr

while [[ $# > 1 ]]
do
    key="$1"

    case $key in
	-p|--prefix)
	    PREFIX="$2"
	    shift
	    ;;
	-d|--dir)
	    DIR="$2"
	    shift
	    ;;
	*)
	# unknown option
	    ;;
    esac
    shift
done

echo prefix: ${PREFIX}
echo dir: ${DIR}

for apk in ${DIR}/*.apk
do
    filename=$(basename "$apk")
    extension="${filename##*.}"
    filename="${filename%.*}"

    apiusagefile="data/apiusage/${PREFIX}${filename}_apiusage.txt"
    echo Checking if $apiusagefile exists
    if [ ! -f $apiusagefile ]; then
	echo Submitting job for apiusage_extraction.sh $apk
	qsub apiusage_extraction.sh $apk
    fi
done