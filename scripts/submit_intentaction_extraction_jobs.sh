#!/bin/bash
#$ -cwd
#$ -pe openmp 1-4
#$ -q free64,pub64
#$ -m as
#$ -j y

while [[ $# > 1 ]]
do
    key="$1"

    case $key in
	-p|--prefix)
	    PREFIX="$2"
	    shift
	    ;;
	-d|--dir)
	    DIR="$2"
	    shift
	    ;;
	*)
	# unknown option
	    ;;
    esac
    shift
done

echo prefix: ${PREFIX}
echo dir: ${DIR}

: <<'END'
apknames=()
for f in data/actions/${PREFIX}*${SUFFIX}.txt
do
    filename=$(basename "$f")
    extension="${filename##*.}"
    filename="${filename%.*}"

    noprefix="${filename##${PREFIX}}"
    apkname="${noprefix%_${SUFFIX}}"
    #echo $apkname
    apknames+=($apkname)
    #echo ${apknames[@]}
done
END


for apk in ${DIR}/*.apk
do
    filename=$(basename "$apk")
    extension="${filename##*.}"
    filename="${filename%.*}"

    actionsfile="data/actions/${PREFIX}${filename}_actions.txt"
    echo Checking if $actionsfile exists
    if [ ! -f $actionsfile ]; then
	echo Submitting job for intentaction_extraction.sh $DIR/$apk
        qsub intentaction_extraction.sh $apk
    fi
done