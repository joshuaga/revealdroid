#!/usr/bin/python

import argparse, sys, random

parser = argparse.ArgumentParser()
parser.add_argument("arff",help="arff file with features to transform")
args = parser.parse_args()

a = open(args.arff,'r')

hitDataLine=False
for line in a:
    tokens = []
    newTokens = []
    if "@data" in line:
        hitDataLine=True
        print line,
    elif hitDataLine:
        tokens = line.split(',')
        newTokens = []
        #random.sample(range(1,30),5)
        for i, token in enumerate(tokens):
            newToken=token
            if i in random.sample(range(1,30),5):
                if token.isdigit():
                    newToken=str(int(token)+1)
                else:
                    newToken=token
            newTokens.append(newToken)
        #print tokens
        #print newTokens
        newLine = ','.join(newTokens)
        print newLine,
    else:
        print line,

