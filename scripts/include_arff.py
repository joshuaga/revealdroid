#!/usr/bin/python

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--arff","-a",help="arff file to select instances from",required=True)
parser.add_argument("--filterfile","-f",help="file with apks to be included",required=True)
args = parser.parse_args()

apks=[]
with open(args.filterfile) as f:
    apks=[line.strip() for line in f.readlines()]

a = open(args.arff,'r')
hitDataLine=False
for line in a:
    if "@data" in line:
        hitDataLine=True
        print line,
    elif hitDataLine:
        #print "This is a data line"
        features=line.split(',')
        if features[0].strip() in apks:
            print line,
    else:
        print line,
