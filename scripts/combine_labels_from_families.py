#!/usr/bin/python

import argparse, re
import weka.core.jvm as jvm
from weka.core.converters import Loader

parser = argparse.ArgumentParser()
parser.add_argument("--families","-f",help="families to be combined",required=True)
parser.add_argument("--combined","-c",help="name of the combined family",required=True)
parser.add_argument("--inarff","-i",help="arff file with instances that should be combined",required=True)
parser.add_argument("--outarff","-o",help="resulting arff file",required=True)
args = parser.parse_args()

print args.families
print args.combined
print args.inarff
print args.outarff

families=list()
with open(args.families) as f:
    families=[x.strip() for x in f.readlines()]

print families

infile=open(args.inarff,'r')

sLabels=families
newLines=list()
classes=set()
for line in infile:
    if line.startswith("@attribute class"):
        classes=[x.strip() for x in re.sub('}','', re.sub('@attribute\s+class\s+{','',line) ).split(',')]
        newLabels=set(classes)
        newLabels-=set(sLabels)
        newLabels=list(newLabels)
        newLabels.append(args.combined.strip())
        newLabels.sort()
        newLine = "@attribute class {" + ','.join(newLabels) + "}\n"
        newLines.append(newLine)
        #print newLine
    else:
        matchesLabel=False
        matchingLabel=''
        for label in sLabels:
            if re.compile(r'.*,\s*'+label.strip()+'\s*$').match(line):
                matchesLabel=True
                matchingLabel=label
        if matchesLabel:
            newLine=line.replace(matchingLabel,args.combined)
            newLines.append(newLine)
            #print newLine,
        else:
            newLines.append(line)
            #print line,

outarff_file = open(args.outarff,'w')
for line in newLines:
    outarff_file.write(line)
outarff_file.close()

jvm.start()
# check if we can load the output arff file Instances without error
loader = Loader(classname="weka.core.converters.ArffLoader")
data = loader.load_file(args.outarff)
jvm.stop()