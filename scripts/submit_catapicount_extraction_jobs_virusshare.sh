#!/bin/bash
#$ -cwd
#$ -pe openmp 1-4
#$ -q free*,pub*
#$ -m beas
#$ -j y
#$ -ckpt blcr
apks=( $(find $ALT_HOME/VirusShare_Android_20140324/ -iname "*.apk") )
sapi_prefix=VirusShare_Android_20140324_
sapi_suffix=_categorized_apicount.txt

for apk in "${apks[@]}"
do
    filename=$(basename "$apk")
    extension="${filename##*.}"
    filename_noext="${filename%.*}"
    #echo $filename_noext
    sapi_file=$sapi_prefix$filename_noext$sapi_suffix
    if [ -f data/catapicount/$sapi_file ]; then
	echo $sapi_file already exists
    else
	echo "Executing: qsub catapicount_extraction.sh $apk"
	qsub catapicount_extraction.sh $apk
    fi
done