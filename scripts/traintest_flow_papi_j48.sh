#$ -pe shared 8
#$ -cwd
#$ -q all-LoPri.q
# Join the standard output & error files into one file (y)[yes] or write to separate files (n)[no]
# The default is n [no]
#$ -j y
#DEBUG_OPTION=-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=y
WORKSPACE=$HOME/git
DEBUG_OPTION=""
java $DEBUG_OPTION -Dfile.encoding=UTF-8 -classpath bin:$WORKSPACE/revealdroid/lib/axml-2.0.jar:$WORKSPACE/revealdroid/lib/slf4j-api-1.7.5.jar:$WORKSPACE/revealdroid/lib/slf4j-simple-1.7.5.jar:$WORKSPACE/revealdroid/lib/weka.jar:$WORKSPACE/revealdroid/lib/jcommander-1.36-SNAPSHOT.jar:$WORKSPACE/soot/classes:$WORKSPACE/jasmin/classes:$WORKSPACE/jasmin/libs/java_cup.jar:$WORKSPACE/heros/bin:$WORKSPACE/heros/guava-14.0.1.jar:$WORKSPACE/heros/slf4j-api-1.7.5.jar:$WORKSPACE/heros/junit.jar:$WORKSPACE/heros/org.hamcrest.core_1.3.0.jar:$WORKSPACE/soot/libs/polyglot.jar:$WORKSPACE/soot/libs/AXMLPrinter2.jar:$WORKSPACE/soot/libs/hamcrest-all-1.3.jar:$WORKSPACE/soot/libs/junit-4.11.jar:$WORKSPACE/soot/libs/dexlib2-2.0.3-dev.jar:$WORKSPACE/soot/libs/util-2.0.3-dev.jar:$WORKSPACE/soot/libs/asm-debug-all-5.0.3.jar:$HOME/Applications/eclipse-jee-luna/plugins/org.junit_4.11.0.v201303080030/junit.jar:$HOME/Applications/eclipse-jee-luna/plugins/org.hamcrest.core_1.3.0.v201303031735.jar:$WORKSPACE/soot-infoflow/bin:$WORKSPACE/soot-infoflow/lib/cos.jar:$WORKSPACE/soot-infoflow/lib/j2ee.jar:$WORKSPACE/soot-infoflow/lib/slf4j-api-1.7.5.jar:$WORKSPACE/soot-infoflow-android/bin:$WORKSPACE/soot-infoflow-android/lib/polyglot.jar:$WORKSPACE/soot-infoflow-android/lib/AXMLPrinter2.jar:$WORKSPACE/soot-infoflow-android/lib/axml-2.0.jar:$WORKSPACE/handleFlowDroid/bin:$HOME/android-sdks/platforms/android-20/android.jar:lib/commons-io-2.4.jar:lib/apk-parser-all.jar revealdroid.classify.TrainTest -r benign_malgenome_virusshare_flows_apiusage_actions.arff -e obfuscated_malgenome_flows_nodupetrans_2way_apiusage_actions.arff -c j48
