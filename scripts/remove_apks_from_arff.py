#!/usr/bin/env python

import argparse
import arff
import os

parser = argparse.ArgumentParser(description="Remove a list of apks from an arff file")
parser.add_argument("--remove","-r",help="file with list of apks to remove",required=True)
parser.add_argument("--arff","-a",help="arff file to remove apks from",required=True)
args = parser.parse_args()

rf = open(args.remove,'r')
rSet = set()
for line in rf:
    rSet.add(line.strip())
#print rSet
#print args.arff
data = arff.load(open(args.arff,'rb'))
for i, datum in reversed(list(enumerate(data['data']))):
    if datum[0] in rSet:
        print "found " + datum[0]
        del data['data'][i]

print len(data['data'])

filename,extension=os.path.splitext(args.arff)
print filename
print extension

newFilename = filename + "_removed" + extension
newF = open(newFilename,'w')
newF.write(arff.dumps(data))
newF.close()

