#!/bin/bash
#$ -cwd
#$ -pe openmp 8-32
#$ -q free*,pub*
#$ -j y

if [ -z ${ALT_HOME+x} ]; then
    export ANDROID_HOME=$HOME/android-sdks/platforms
else
    export ANDROID_HOME=$ALT_HOME/android-sdks/platforms
fi
: ${RD_WORKSPACE:=$PWD/../}
export RD_WORKSPACE
: ${RD_HOME:=$PWD}
export RD_HOME
#DEBUG_OPTION=-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=y

echo Analyzing $1

DEBUG_OPTION=""
java $DEBUG_OPTION -classpath $RD_WORKSPACE/revealdroid/bin:$RD_WORKSPACE/revealdroid/lib/axml-2.0.jar:$RD_WORKSPACE/revealdroid/lib/slf4j-api-1.7.5.jar:$RD_WORKSPACE/revealdroid/lib/weka.jar:$RD_WORKSPACE/revealdroid/lib/jcommander-1.36-SNAPSHOT.jar:$RD_WORKSPACE/revealdroid/lib/commons-io-2.4.jar:$RD_WORKSPACE/revealdroid/lib/apk-parser-all.jar:$RD_WORKSPACE/revealdroid/lib/libsvm.jar:$RD_WORKSPACE/soot/classes:$RD_WORKSPACE/jasmin/classes:$RD_WORKSPACE/jasmin/libs/java_cup.jar:$RD_WORKSPACE/heros/bin:$RD_WORKSPACE/heros/guava-14.0.1.jar:$RD_WORKSPACE/heros/junit.jar:$RD_WORKSPACE/heros/org.hamcrest.core_1.3.0.jar:$RD_WORKSPACE/soot/libs/polyglot.jar:$RD_WORKSPACE/soot/libs/AXMLPrinter2.jar:$RD_WORKSPACE/soot/libs/hamcrest-all-1.3.jar:$RD_WORKSPACE/soot/libs/junit-4.11.jar:$RD_WORKSPACE/soot/libs/dexlib2-2.0.3-dev.jar:$RD_WORKSPACE/soot/libs/util-2.0.3-dev.jar:$RD_WORKSPACE/soot/libs/asm-debug-all-5.0.3.jar:/home/joshua/Applications/eclipse-jee-luna/plugins/org.junit_4.11.0.v201303080030/junit.jar:/home/joshua/Applications/eclipse-jee-luna/plugins/org.hamcrest.core_1.3.0.v201303031735.jar:$RD_WORKSPACE/soot-infoflow/bin:$RD_WORKSPACE/soot-infoflow/lib/cos.jar:$RD_WORKSPACE/soot-infoflow/lib/j2ee.jar:$RD_WORKSPACE/soot-infoflow-android/bin:$RD_WORKSPACE/soot-infoflow-android/lib/polyglot.jar:$RD_WORKSPACE/soot-infoflow-android/lib/AXMLPrinter2.jar:$RD_WORKSPACE/soot-infoflow-android/lib/commons-io-2.4.jar:$RD_WORKSPACE/soot-infoflow-android/lib/axml-2.0.jar:$RD_WORKSPACE/handleFlowDroid/bin:$RD_WORKSPACE/revealdroid/lib/android.jar:$RD_WORKSPACE/revealdroid/lib/logback-core-1.1.2.jar:$RD_WORKSPACE/revealdroid/lib/logback-classic-1.1.2.jar:../soot/libs/dexlib2-2.1.0-dev.jar:../soot/libs/util-2.1.0-dev.jar:../heros/guava-18.0.jar revealdroid.enduser.cli.AnalyzeSingleAppForReputationUsingCatApicount $1
