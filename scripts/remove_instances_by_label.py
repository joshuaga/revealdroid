#!/usr/bin/python

import argparse, re
import weka.core.jvm as jvm
from weka.core.converters import Loader

parser = argparse.ArgumentParser()
parser.add_argument("--labels","-l",nargs="+",required=False,help="labels for instances that should be removed")
parser.add_argument("--inarff","-i",required=True,help="input arff file with instances that should be removed")
parser.add_argument("--outarff","-o",required=True,help="output arff file to store removed instances")
args = parser.parse_args()

print args.labels
print args.inarff

infile = open(args.inarff,'r')

sLabels=args.labels

newLines=list()
classes=set()
for line in infile:
    if line.startswith("@attribute class"):
        classes=[x.strip() for x in re.sub('}','', re.sub('@attribute\s+class\s+{','',line) ).split(',')]
        newLabels=set(classes)
        newLabels-=set(sLabels)
        newLabels=list(newLabels)
        newLabels.sort()
        newLine = "@attribute class {" + ','.join(newLabels) + "}\n"
        newLines.append(newLine)
        #print newLine
    else:
        matchesLabel=False
        for label in sLabels:
            if re.compile(r'.*,\s*'+label.strip()+'\s*$').match(line):
                matchesLabel=True
        if not matchesLabel:           
            newLines.append(line)
            #print line,

outarff_file = open(args.outarff,'w')
for line in newLines:
    outarff_file.write(line)
outarff_file.close()

jvm.start()
# check if we can load the output arff file Instances without error
loader = Loader(classname="weka.core.converters.ArffLoader")
data = loader.load_file(outarff_file)
jvm.stop()

    