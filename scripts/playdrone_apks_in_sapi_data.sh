#!/bin/bash
#$ -cwd
#$ -pe openmp 1-4
#$ -q free64,pub64
#$ -m beas
#$ -j y
#$ -ckpt blcr
datadir=$1
sapifiles=( $(find $1 -iname "playdrone_apps_*_categorized_apicount.txt") )
sapi_prefix=playdrone_apps_
sapi_suffix=_categorized_apicount.txt

for sapifile in $1/playdrone_apps_*_categorized_apicount.txt
do
    filename=$(basename "$sapifile")
    noprefix=${filename#${sapi_prefix}}
    apkname=${noprefix%${sapi_suffix}}
    #echo $sapifile
    #echo $noprefix
    echo $apkname
done