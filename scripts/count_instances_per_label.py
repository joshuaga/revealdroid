#!/usr/bin/python

import argparse, operator

parser = argparse.ArgumentParser()
parser.add_argument('arff',help='input arff file')
args = parser.parse_args()

a = open(args.arff,'r')

family_counts=dict()
hitDataLine = False
for line in a:
    if '@data' in line:
        hitDataLine=True
    elif hitDataLine:
            tokens = line.split(',')
            family=tokens[-1].strip()
            #print family
            #print family_counts
            if family in family_counts:
                family_counts[family]+=1
            else:
                family_counts[family]=1

for family,count in sorted(family_counts.items(),key=operator.itemgetter(1)):
    print family, count
            
