#!/bin/bash
#$ -cwd
#$ -pe openmp 1-4
#$ -q free*,pub*
#$ -m beas
#$ -j y
#DEBUG_OPTION=-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=y
export ANDROID_HOME=$ALT_HOME/android-sdks/platforms

while [[ $# > 1 ]]
do
    key="$1"

    case $key in
	-a|--arff)
	    ARFF="$2"
	    shift
	    ;;
	-d|--dir)
	    DIR="$2"
	    shift
	    ;;
	-p|--prefix)
	    PREFIX="$2"
	    shift
	    ;;
	-s|--suffix)
	    SUFFIX="$2"
	    shift
	    ;;
	*)
	# unknown option
	    ;;
    esac
    shift
done

echo arff: ${ARFF}
echo dir: ${DIR}
echo prefix: ${PREFIX}
echo suffix: ${SUFFIX}

OPTIONAL_ARGS=""
if [[ -z ${PREFIX+x} || -z ${SUFFIX+x} ]]; then
    OPTIONAL_ARGS=""
else
    OPTIONAL_ARGS="-p ${PREFIX} -s ${SUFFIX}"
fi

DEBUG_OPTION=""
java $DEBUG_OPTION -Dfile.encoding=UTF-8 -classpath bin:$ALT_HOME/workspace/revealdroid/lib/axml-2.0.jar:$ALT_HOME/workspace/revealdroid/lib/slf4j-api-1.7.6.jar:$ALT_HOME/workspace/revealdroid/lib/weka.jar:$ALT_HOME/workspace/revealdroid/lib/jcommander-1.36-SNAPSHOT.jar:$ALT_HOME/workspace/soot/classes:$ALT_HOME/workspace/jasmin/classes:$ALT_HOME/workspace/jasmin/libs/java_cup.jar:$ALT_HOME/workspace/heros/bin:$ALT_HOME/workspace/heros/guava-14.0.1.jar:$ALT_HOME/workspace/heros/junit.jar:$ALT_HOME/workspace/heros/org.hamcrest.core_1.3.0.jar:$ALT_HOME/workspace/soot/libs/polyglot.jar:$ALT_HOME/workspace/soot/libs/AXMLPrinter2.jar:$ALT_HOME/workspace/soot/libs/hamcrest-all-1.3.jar:$ALT_HOME/workspace/soot/libs/junit-4.11.jar:$ALT_HOME/workspace/soot/libs/dexlib2-2.0.3-dev.jar:$ALT_HOME/workspace/soot/libs/util-2.0.3-dev.jar:$ALT_HOME/workspace/soot/libs/asm-debug-all-5.0.3.jar:$ALT_HOME/Applications/eclipse-jee-luna/plugins/org.junit_4.11.0.v201303080030/junit.jar:$ALT_HOME/Applications/eclipse-jee-luna/plugins/org.hamcrest.core_1.3.0.v201303031735.jar:$ALT_HOME/workspace/soot-infoflow/bin:$ALT_HOME/workspace/soot-infoflow/lib/cos.jar:$ALT_HOME/workspace/soot-infoflow/lib/j2ee.jar:$ALT_HOME/workspace/soot-infoflow-android/bin:$ALT_HOME/workspace/soot-infoflow-android/lib/polyglot.jar:$ALT_HOME/workspace/soot-infoflow-android/lib/AXMLPrinter2.jar:$ALT_HOME/workspace/soot-infoflow-android/lib/axml-2.0.jar:$ALT_HOME/workspace/handleFlowDroid/bin:$ALT_HOME/android-sdks/platforms/android-20/android.jar:lib/commons-io-2.4.jar:lib/apk-parser-all.jar:lib/logback-core-1.1.2.jar:lib/logback-classic-1.1.2.jar revealdroid.features.apiusage.ApiUsageFeaturesAdder -a ${DIR} -d ${ARFF} ${OPTIONAL_ARGS}