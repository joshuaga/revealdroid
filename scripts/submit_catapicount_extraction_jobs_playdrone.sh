#!/bin/bash
#$ -cwd
#$ -pe openmp 1-4
#$ -q free64,pub64
#$ -m beas
#$ -j y
#$ -ckpt blcr
apks=( $(find $ALT_HOME/playdrone_apps/ -iname "*.apk") )
sapi_prefix=playdrone_apps_
sapi_suffix=_categorized_apicount.txt

for apk in "${apks[@]}"
do
    filename=$(basename "$apk")
    extension="${filename##*.}"
    filename_noext="${filename%.*}"
    #echo $filename_noext
    sapi_file=$sapi_prefix$filename_noext$sapi_suffix
    if [ -f data/catapicount/$sapi_file ]; then
	echo $sapi_file already exists
    else
	echo "Executing: qsub catapicount_extraction.sh $apk"
	qsub catapicount_extraction.sh $apk
    fi
done