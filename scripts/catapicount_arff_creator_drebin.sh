#!/bin/bash
#$ -cwd
#$ -pe openmp 8-64
#$ -q free*,pub64
#$ -m beas
#$ -j y
DEBUG_OPTION=-agentlib:jdwp=transport=dt_socket,address=9001,server=y,suspend=y
export RD_HOME=.
module load java/1.8.0.51
#DEBUG_OPTION=""
java $DEBUG_OPTION -Dfile.encoding=UTF-8 -classpath bin:./lib/axml-2.0.jar:./lib/slf4j-api-1.7.5.jar:./lib/slf4j-simple-1.7.6.jar:./lib/weka.jar:./lib/jcommander-1.36-SNAPSHOT.jar:../soot/classes:../jasmin/classes:../jasmin/libs/java_cup.jar:../heros/bin:../heros/guava-14.0.1.jar:../heros/slf4j-api-1.7.5.jar:../heros/junit.jar:../heros/org.hamcrest.core_1.3.0.jar:../soot/libs/polyglot.jar:../soot/libs/AXMLPrinter2.jar:../soot/libs/hamcrest-all-1.3.jar:../soot/libs/junit-4.11.jar:../soot/libs/dexlib2-2.0.3-dev.jar:../soot/libs/util-2.0.3-dev.jar:../soot/libs/asm-debug-all-5.0.3.jar:$HOME/Applications/eclipse-jee-luna/plugins/org.junit_4.11.0.v201303080030/junit.jar:$HOME/Applications/eclipse-jee-luna/plugins/org.hamcrest.core_1.3.0.v201303031735.jar:../soot-infoflow/bin:../soot-infoflow/lib/cos.jar:../soot-infoflow/lib/j2ee.jar:../soot-infoflow-android/bin:../soot-infoflow-android/lib/polyglot.jar:../soot-infoflow-android/lib/AXMLPrinter2.jar:../soot-infoflow-android/lib/axml-2.0.jar:../handleflowdroid/bin:$ALT_HOME/android-sdks/platforms/android-20/android.jar:lib/commons-io-2.4.jar:lib/apk-parser-all.jar:lib/logback-classic-1.1.2.jar:lib/logback-core-1.1.2.jar revealdroid.features.apiusage.CategorizedApiCountArffCreator -a $ALT_HOME/VirusShare_Android_20140324 -c $ALT_HOME/workspace/revealdroid/data/catapicount/drebin -r drebin_catapicount.arff
