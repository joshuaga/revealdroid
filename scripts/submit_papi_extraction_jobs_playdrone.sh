#!/bin/bash
#$ -cwd
#$ -pe openmp 1-4
#$ -q free64,pub64
#$ -m beas
#$ -j y
#$ -ckpt blcr
apks=( $(find $ALT_HOME/playdrone_apps/ -iname "*.apk") )
papi_prefix=playdrone_apps_
papi_suffix=_apiusage.txt

for apk in "${apks[@]}"
do
    filename=$(basename "$apk")
    extension="${filename##*.}"
    filename_noext="${filename%.*}"
    #echo $filename_noext
    papi_file=$papi_prefix$filename_noext$papi_suffix
    if [ -f data/catapicount/$papi_file ]; then
	echo $papi_file already exists
    else
	echo "Executing: qsub catapicount_extraction.sh $apk"
	qsub papi_extraction.sh $apk
    fi
done