#!/bin/bash
#$ -cwd
#$ -pe openmp 2
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/revealdroid_logs/enec_virusshare/
#$ -e /share/seal/joshug4/revealdroid_logs/enec_virusshare/
#$ -t 1-22592
#$ -r y

module load anaconda/2.7-4.3.1
SEEDFILE=virusshare_apk_paths.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/enec.sh $SEED
