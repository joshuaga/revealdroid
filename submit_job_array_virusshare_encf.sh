#!/bin/bash
#$ -cwd
#$ -pe openmp 4
#$ -q seal,free*,pub*
#$ -j y
#$ -t 1-10000
#$ -r y
module load enthought_python/7.3.2
SEEDFILE=virusshare_seeds.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/extract_native_code_files.py $SEED
