#!/usr/bin/env python

import argparse
import h5py
import numpy as np
import os

from compare_labels import create_apk_label_dict

def create_apk_indices_dict(apk_names):
    apk_name_indices=dict()
    for i,name in enumerate(apk_names):
        apk_name_indices[name]=i
    return apk_name_indices

def main():
    parser = argparse.ArgumentParser(description='replace labels in one HDF5 file with labels from another HDF5 file')
    parser.add_argument('-f1',help='first HDF5 input file',required=True)
    parser.add_argument('-l1',help='label to copy from first HDF5 file',required=True)
    parser.add_argument('-f2',help='second HDF5 input file',required=True)
    parser.add_argument('-l2',help='label to copy from second HDF5 file',required=True)
    args = parser.parse_args()
    
    h5f1 = h5py.File(args.f1)
    h5f2 = h5py.File(args.f2)
    
    apk_names1 = h5f1['apk_names'][:]
    apk_names2 = h5f2['apk_names'][:]
    y1 = h5f1['y'][:]
    y2 = h5f2['y'][:]
    
    apk_labels1 = create_apk_label_dict(apk_names1,y1)
    apk_labels2 = create_apk_label_dict(apk_names2,y2)

    apk_name_indices1 = create_apk_indices_dict(apk_names1)
    apk_name_indices2 = create_apk_indices_dict(apk_names2)
    
    y2_new = np.copy(y2)
    for apk_name,label in apk_labels1.items():
        if not apk_name in apk_labels2:
            print '{} does not contain {}'.format(args.f2,apk_name)
            continue
        if label == args.l1 and apk_labels2[apk_name] == args.l2:
            print 'Copying {} in {} to {} in {}'.format(args.l1,args.f1,args.l2,args.f2)
            y2_new[ apk_name_indices2[apk_name]  ] = args.l1

    #in_basename = os.path.splitext(args.f2)[0]
    #new_filename=in_basename + '_copied-labels.hdf'
    #print 'Storing to new file: {}'.format(new_filename)
    #new_f = h5py.File(new_filename,'w')
    #h5f2.copy('X',new_f)
    #h5f2.copy('apk_names',new_f)
    #h5f2.copy('fnames',new_f)
    dt = h5py.special_dtype(vlen=unicode)
    del h5f2['y']
    h5f2.create_dataset('y',data=np.array(y2_new),dtype=dt)
    h5f2.close()

    h5f1.close()

if __name__ == '__main__':
    main()
