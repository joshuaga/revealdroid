#!/bin/bash
#$ -cwd
#$ -pe openmp 4
#$ -q seal,free64,pub64
#$ -j y
#$ -t 1-1109
#$ -r y
#$ -ckpt restart
module load enthought_python/7.3.2
SEEDFILE=ref_omg_apps_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/extract_native_code_files.py $SEED
