#!/bin/bash
#$ -cwd
#$ -q free*,pub*
#$ -o /share/seal/joshug4/revealdroid_logs/create_dataset/
#$ -e /share/seal/joshug4/revealdroid_logs/create_dataset/
#$ -l mem_size=512
#$ -ckpt blcr

module load anaconda/2.7-4.3.1
./create_dataset_from_feature_files.py $@
