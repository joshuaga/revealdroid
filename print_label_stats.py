#!/usr/bin/env python

import argparse
import h5py
import pprint

parser = argparse.ArgumentParser(description='print label stats in HDF file')
parser.add_argument('-f',help='HDF5 input file',required=True)
args = parser.parse_args()

h5f = h5py.File(args.f)
y = h5f['y'][:]

label_set = set(y)
unknown_count=0
labels = dict()
for label in y:
    if  label in labels:
        labels[label] = labels[label] + 1
    else:
        labels[label] = 1

num_samples=h5f['X'].shape[0]
print 'total samples: {}'.format(num_samples)
pprint.pprint(labels)
        

