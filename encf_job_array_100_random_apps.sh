#!/bin/bash
#$ -cwd
#$ -pe openmp 1
#$ -q seal
#$ -j y
#$ -t 1-100
#$ -r y
module load enthought_python/7.3.2
SEEDFILE=100_random_apps_list.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
time /share/seal/joshug4/workspace/revealdroid/extract_native_code_files.py $SEED
