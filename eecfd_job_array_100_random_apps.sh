#!/bin/bash
#$ -cwd
#$ -q seal
#$ -j y
#$ -t 1-100
#$ -r y
module load enthought_python/7.3.2
SEEDFILE=encf_logs_list_for_100_random_apps.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
time /share/seal/joshug4/workspace/revealdroid/extract_external_calls_from_disassembly.py $SEED
