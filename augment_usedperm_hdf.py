#!/usr/bin/env python

import h5py
import argparse

def main():
    parser = argparse.ArgumentParser(description='Add usedperm# to beginning of all features names in HDF5 file')
    parser.add_argument('--hdf',help='HDF5 file',required=True)
    args = parser.parse_args()
    
    h5f = h5py.File(args.hdf)
    fnames = h5f['fnames'][:]
    fnames = ['usedperm#{}'.format(fname).encode('utf8') for fname in fnames]
    
    del h5f['fnames']
    h5f.create_dataset('fnames',data=fnames,dtype=h5py.special_dtype(vlen=unicode))
    h5f.close()

if __name__ == '__main__':
    main()
