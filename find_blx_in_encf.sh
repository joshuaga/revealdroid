#!/bin/bash
#$ -cwd
#disabled -pe openmp 2
#$ -q seal
#$ -j y

find . -maxdepth 1 -name "encf.sh.o*" -size +400c -exec grep -Hn " blx " {} \;