#!/usr/bin/env python

import argparse
import re
import os

parser = argparse.ArgumentParser(description='Extract external calls from disassembly output file')
parser.add_argument('disasm_file',help='file with objdump disassembly output')
args = parser.parse_args()
print args.disasm_file

disasm_f = open(args.disasm_file,'r')

apk_name=None
calls = dict()
for line in disasm_f:
    if line.strip().endswith('.apk'):
        apk_name=os.path.basename(line.strip())
        print "apk: " + apk_name
    m_ext_call = re.match(r".*\s+(b|bl|blx)\s+.*<(.+)@plt.+>.*",line) # if I see a branch instruction referencing the PLT
    #if not m_ext_call:
        #m_plt_label = re.match(r".*\s+<(.+)@plt.*>",line) # for handling the PLT section directly
    if m_ext_call:
         #print m_ext_call.group(2)
        if m_ext_call.group(2) in calls:
            calls[m_ext_call.group(2)] += 1
        else:
            calls[m_ext_call.group(2)] = 1
    # if m_plt_label:
    #     if m_plt_label.group(1) in calls:
    #         calls[m_plt_label.group(1)] += 1
    #     else:
    #         calls[m_plt_label.group(1)] = 1
disasm_f.close()

for k,v in calls.iteritems():
    print "{}: {}".format(k,v)

apk_filename,apk_ext=os.path.splitext(apk_name)
nec_out_file=apk_filename+"_nec.txt"
nec_dir=os.path.join("data","native_external_calls")
if not os.path.exists(nec_dir):
    os.makedirs(nec_dir)

nec_out_f=open(os.path.join(nec_dir,nec_out_file),'w')
nec_out_f.write("apk_name," + apk_name + "\n")

for k,v in calls.iteritems():
    nec_out_f.write( "{},{}".format(k,v) + "\n")

nec_out_f.close()
