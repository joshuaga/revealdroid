#!/bin/bash
#$ -cwd
#$ -pe openmp 8-64
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/revealdroid_logs/
#$ -e /share/seal/joshug4/revealdroid_logs/
#$ -ckpt blcr

module load anaconda/2.7-4.3.1
./create_drebin_dataset.py $@
