#!/bin/bash
#$ -cwd
#$ -pe openmp 1
#$ -q seal,free*,pub*
#$ -o /share/seal/joshug4/revealdroid_logs/
#$ -e /share/seal/joshug4/revealdroid_logs/
#$ -t 1-1188
#$ -ckpt restart

module load anaconda/2-2.3.0
SEEDFILE=1188_transformed_malgenome_app_paths.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/extract_prn_features.sh  $SEED

