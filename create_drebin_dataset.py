#!/usr/bin/env python

import h5py
import numpy as np
import os
import time
import sys
from multiprocessing.dummy import Pool 
from functools import partial
from scipy.sparse import coo_matrix
from scipy.sparse import vstack

removable_prefixes=['gplay_pure_apps_','drebin-all_','Malware_VirusShare_','android_exploit_apks_'] # prefixes to strip from beginning of feature filenames
apks_to_features=dict()
benign_apps=set()

def store_hdf_data(new_hdf_filename,x,y,n,f):
    new_f = h5py.File(new_hdf_filename,'w')
    new_f.create_dataset('X',data=np.array(x),dtype=np.int)
    dt = h5py.special_dtype(vlen=unicode)
    new_f.create_dataset('y',data=np.array(y),dtype=dt)
    new_f.create_dataset('apk_names',data=np.array(n),dtype=dt)
    new_f.create_dataset('fnames',data=np.array(f),dtype=dt)
    new_f.close()

def fill_X(hash_pairs,X,feature_names):
    #for pair in hash_pairs:
    #   print '{} {}'.format(type(pair),pair)
    #for (hash_idx,hash_name) in hash_pairs:
    #    print '{} {}'.format(hash_idx,hash_name)

    print 'X.shape in fill_X: {}'.format(X.shape)
    #print feature_names
    # for hash_idx,hash_name in hash_pairs:
    #     for feat_idx,feat_name in enumerate(feature_names):
    #         if feat_name in apks_to_features[hash_name]:
    #             X[hash_idx,feat_idx] = apks_to_features[hash_name][feat_name]
    #         else:
    #             X[hash_idx,feat_idx] = 0

def func(hash_pairs):
    for hash_idx,hash_name in hash_pairs:
        print '{} {}'.format(hash_idx,hash_name)

def func_hash_only(hash_indices,hashes):
    for hash_idx in hash_indices:
        print hashes[hash_idx]
    return 0
    
def main():
    prog_start_time=time.time()
    perm_dir = 'data/perm/'
    used_perm_dir = 'data/used_perm/'
    netaddr_dir = 'data/netaddr/'
    perm_type = 'perm'
    used_perm_type = 'used_perm'
    netaddr_type = 'netaddr'

    feat_type_tuples=[(perm_type,perm_dir,'_perm.txt'),(used_perm_type,used_perm_dir,'_usedperm.txt'),(netaddr_type,netaddr_dir,'_netaddr.txt')]

    start_time=time.time()
    for feat_type,feat_dir,suffix in feat_type_tuples:
        extract_features_from_dir(feat_dir,suffix,feat_type)
    print 'time to extract features from directories: {}'.format(time.time()-start_time)
    sys.stdout.flush()

    start_time=time.time()
    all_hashes=apks_to_features.keys()
    print 'type of all_hashes: {}'.format(type(all_hashes))
    for hash_name in all_hashes:
        print hash_name
    all_feature_names = set()
    for hash_id,features in apks_to_features.items():
        all_feature_names = all_feature_names.union( set(features.keys()) )
    print 'time to construct feature names: {}'.format(time.time()-start_time)
    sys.stdout.flush()
    
    start_time=time.time()
    fnames = [fname for fname in all_feature_names]
    print 'time to convert feature names to list:{}'.format(time.time()-start_time)
    sys.stdout.flush()

    start_time=time.time()
    X = coo_matrix( ( len(all_hashes),len(all_feature_names) ), dtype = np.int).tolil()
    print 'X.shape in main(): {}'.format(X.shape)
    hash_pairs=[ (hash_idx,hash_name) for (hash_idx,hash_name) in enumerate(all_hashes)]
    #for hash_idx,hash_name in enumerate(all_hashes):
    #    print hash_idx, hash_name
    #    hash_pairs.append((hash_idx,hash_name))
    pool = Pool()
    #pool.map( partial(fill_X,X=X,feature_names=all_feature_names),hash_pairs )
    random_crap=['a7sdf7n','jb9827344j','asd974n']
    #random_crap=[5,10,15]
    for r in random_crap:
        print r
    #pool.map(func_hash_only,sorted(all_hashes))
    #pool.map(partial(func_hash_only,hashes=random_crap),range(len(random_crap)))
    pool.close()
    return
    # for hash_idx,hash_name in enumerate(all_hashes):
    #     for feat_idx,feat_name in enumerate(all_feature_names):
    #         if feat_name in apks_to_features[hash_name]:
    #             X[hash_idx,feat_idx] = apks_to_features[hash_name][feat_name]
    #         else:
    #             X[hash_idx,feat_idx] = 0
    print 'time to construct X dataset: {}'.format(time.time()-start_time)
    sys.stdout.flush()

    start_time=time.time()
    apk_names=[]
    y=[]
    for hash_name in all_hashes:
        apk_names.append(hash_name)
        if hash_name in benign_apps:
            y.append('Benign')
        else:
            y.append('unknown')
    print 'time to construct apk_names and y datasets: {}'.format(time.time()-start_time)
    sys.stdout.flush()
    
    start_time=time.time()
    timestr = time.strftime("%Y%m%d-%H%M%S")
    new_hdf_filename = 'drebin_feature_dataset_{}.hdf'.format(timestr)
    store_hdf_data(new_hdf_filename,X.toarray(),y,apk_names,fnames)
    print 'time to store HDF5 file to {}: {}'.format(new_hdf_filename,time.time()-start_time)
    sys.stdout.flush()

    print 'total execution time: {}'.format(time.time()-prog_start_time)
    sys.stdout.flush()

def extract_features_from_dir(feature_dir,feature_file_suffix,feat_type):
    file_limit=3
    file_count = 1
    for filename in os.listdir(feature_dir):
        if file_count >= file_limit:
            break
        path = '{}/{}'.format(feature_dir,filename)
        with open(path,'r') as f:
            hash_id=None
            found_prefix=False
            for prefix in removable_prefixes:
                if filename.startswith(prefix):
                    hash_id = filename.replace(prefix,'')
                    found_prefix=True
            if not found_prefix:
                print 'File {} does not have a prefix I\'m expecting, so I\'m skipping it'.format(filename)
                continue
            hash_id = hash_id.replace(feature_file_suffix,'')
            if filename.startswith('gplay_pure_apps_'):
                benign_apps.add(hash_id)

            if hash_id in apks_to_features:
                features = apks_to_features[hash_id]
            else:
                features = dict()
            for line in f:
                line = line.strip()
                if not line:
                    continue
                tokens = line.split()
                if len(tokens) == 2:
                    feat_name = '{}#{}'.format( feat_type, tokens[0].strip() ) 
                    feat_val = 1
                elif len(tokens) == 1:
                    feat_name = '{}#{}'.format( feat_type, tokens[0].strip() )
                    feat_val = 1
                else:
                    raise TypeError('WTF is this feature from {}? {}'.format(path,line))
                features[feat_name]=feat_val
            apks_to_features[hash_id]=features
            file_count=file_count+1
    return apks_to_features

if __name__ == '__main__':
    main()
