#!/usr/bin/env python

import argparse
import os
import h5py
import numpy as np
import scipy.sparse
import chardet
import time
import re
import sys

from sklearn.feature_extraction import DictVectorizer
from scipy import sparse

import tables as tb
from numpy import array

def store_sparse_mat(m, name, store):
    msg = "This code only works for csc matrices"
    assert(m.__class__ == sparse.csc.csc_matrix), msg
    with tb.openFile(store,'a') as f:
        for par in ('data', 'indices', 'indptr', 'shape'):
            full_name = '%s_%s' % (name, par)
            try:
                n = getattr(f.root, full_name)
                n._f_remove()
            except AttributeError:
                pass

            arr = array(getattr(m, par))
            atom = tb.Atom.from_dtype(arr.dtype)
            ds = f.createCArray(f.root, full_name, atom, arr.shape)
            ds[:] = arr

def load_sparse_mat(name, store):
    with tb.openFile(store) as f:
        pars = []
        for par in ('data', 'indices', 'indptr', 'shape'):
            pars.append(getattr(f.root, '%s_%s' % (name, par)).read())
    m = sparse.csc_matrix(tuple(pars[:3]), shape=pars[3])
    return m

def create_nonnative_dataset(files):
    dataset=[]
    for f in files:
        fv=dict()
        apk_name,label=extract_metadata_from_path(f)
        fv['apk_name']=apk_name.replace('_fapi.txt','').replace('exploit_apks_','').replace('Benign_','')
        #print 'curr apk name: {}'.format(apk_name)
        for line in open(f):
            tokens = line.rstrip('\n').split(',')
            pkg = tokens[0]
            count = tokens[1]
            #print 'pkg: {}, count: {}'.format(pkg,count)
            fv[pkg]=int(count)

        fv['label']=label
        dataset.append(fv)
    return dataset

def create_native_dataset(files):
    dataset=[]
    for f in files:
        fv=dict()
        for line in open(f):
            tokens = line.rstrip('\n').split(',')
            feat_name = tokens[0]
            feat_val = tokens[1]
            #print 'feat_name encoding: {}'.format(chardet.detect(feat_name)['encoding'])
            if chardet.detect(feat_name)['encoding'] is None:
                feat_name = 'native_function_name_with_unknown_encoding'
            feat_name = feat_name.encode('utf8')
            #print 'feat_name: {}, feat_val: {}'.format(feat_name,feat_val)
            if feat_name == 'apk_name':
                fv[feat_name]=feat_val
            else:                
                fv[feat_name]=int(feat_val)

        fv['label']='unknown'
        dataset.append(fv)
    return dataset

removable_prefixes=['gplay_pure_apps_','drebin-all_','Malware_VirusShare_','android_exploit_apks_']
def strip_prefixes(f):
    basename = os.path.splitext(os.path.basename(f))[0]
    for prefix in removable_prefixes:
        if basename.startswith(prefix):
            basename = basename.replace(prefix,'')
    return basename

def create_netaddr_dataset(files):
    dataset=[]
    FILE_LIMIT=sys.maxint
    NON_UNICODE_NETADDR = 'NON_UNICODE_NET_ADDR'
    file_count=0
    for f in files:
        if file_count >= FILE_LIMIT:
            break
        fv=dict()
        basename = strip_prefixes(f)
        apk_name = basename.replace('_netaddr','').replace('Benign_','')
        fv['apk_name']=apk_name
        for line in open(f):
            tokens = line.rstrip('\n').split()
            feat_name = tokens[0]
            try:
                if chardet.detect(feat_name)['encoding'] is None:
                    feat_name = NON_UNICODE_NETADDR
                feat_name = feat_name.encode('utf8')
            except UnicodeDecodeError:
                feat_name = NON_UNICODE_NETADDR
                feat_name = feat_name.encode('utf8')
            fv[feat_name]=1
        fv['label']='unknown'
        dataset.append(fv)
        file_count=file_count+1
    return dataset

def create_boolean_dataset(files):
    dataset=[]
    FILE_LIMIT=sys.maxint
    removable_suffixes=['_perm','_usedperm','_used_perm','_reqperm','_req_perm']
    file_count=0
    for f in files:
        if file_count >= FILE_LIMIT:
            break
        fv=dict()
        basename = strip_prefixes(f)
        apk_name = basename.replace('Benign_','')
        for suffix in removable_suffixes:
            apk_name = apk_name.replace(suffix,'')
        fv['apk_name']=apk_name
        for line in open(f):
            feat_name = line.strip()
            if feat_name  == '':
                continue
            fv[feat_name]=1
        fv['label']='unknown'
        dataset.append(fv)
        file_count=file_count+1
    return dataset


def main():
    prog_start_time=time.time()
    parser = argparse.ArgumentParser(description='create a dataset from PAPI feature files')
    parser.add_argument('--file-list',help='file containing a list of PAPI feature files and their locations',required=True)
    parser.add_argument('--outfile',help='name of file to store dataset in',required=True)
    parser.add_argument('--feat-type',help='d is for default, which includes package, method, and reflection; v is for native; a is for network addresses; b is for boolean features (such as used permission and requested permissions)',required=True)
    args = parser.parse_args()
    
    files = [line.rstrip('\n') for line in open(args.file_list)]

    print 'populating dataset from {}'.format(args.file_list)
    dataset = None
    if args.feat_type == 'd':
        dataset = create_nonnative_dataset(files)
    elif args.feat_type == 'v':
        dataset = create_native_dataset(files)
    elif args.feat_type == 'a':
        dataset = create_netaddr_dataset(files)
    elif args.feat_type == 'b':
        dataset = create_boolean_dataset(files)
    else:
        print 'You need to specify a feature type using --feat-type'
        parser.print_help()
        return
        
    apk_names = []
    y = []
    for f in dataset:
        for n,v in f.iteritems():
            if n == 'apk_name':
                apk_names.append(v)
            if n == 'label':
                y.append(v)

    new_dataset=[]
    for f in dataset:
        f.pop('apk_name')
        f.pop('label')
        new_dataset.append(f)
        
    vec=DictVectorizer()
    X=vec.fit_transform(new_dataset)
    fnames=vec.get_feature_names()
    new_fnames = []
    nonascii_encoding_feature_count=0
    for feat_name in fnames:
        if chardet.detect(feat_name)['encoding'].strip() != 'ascii' :
            orig_feat_name=feat_name
            feat_name='nonascii_encoding_feature{}'.format(nonascii_encoding_feature_count) 
            print 'Detected feature name with nonascii encoding. Found encoding {}. The feature {} has been renamed to {}'.format(chardet.detect(feat_name)['encoding'],orig_feat_name,feat_name)
        new_fnames.append(feat_name)
        nonascii_encoding_feature_count=nonascii_encoding_feature_count+1
        
        
    fnames=[f.encode('utf8') for f in new_fnames]
    #X=scipy.sparse.csc_matrix(np.array(dataset))

    #print X
    #print apk_names
    #print y
    #print 'apk_names element type: {}'.format(type(apk_names[0]))
    #print 'fnames element type: {}'.format(type(fnames[0]))
    #print 'fnames as numpy array: {}'.format(np.array(fnames))

    print 'dataset shape: {}'.format(X.shape)
    print 'storing as HDF5 file to {}'.format(args.outfile)
    h5f = h5py.File(args.outfile,'w')
    h5f.create_dataset('X',data=X.toarray(),dtype=int)
    dt = h5py.special_dtype(vlen=unicode)
    h5f.create_dataset('y',data=np.array(y),dtype=dt)
    h5f.create_dataset('apk_names',data=np.array(apk_names),dtype=dt)
    h5f.create_dataset('fnames',data=np.array(fnames),dtype=dt)    
    h5f.close()
    #f.write(json.dumps(dataset))
    print 'script runtime: {}'.format(time.time()-prog_start_time)

def extract_metadata_from_path(pathname):
    filename = os.path.basename(pathname)
    label = filename[:filename.find('_')]
    apk_name=filename.replace(label+'_','')
    if apk_name.strip() == '' or apk_name.startswith('_') or apk_name.endswith('.txt'):
        p = re.compile('(.*)_(.*)\.txt')
        m = p.match(filename)
        apk_name = m.group(1)
        label = 'unknown'
    if apk_name.startswith('VirusShare'):
        apk_name = apk_name.replace('VirusShare_','')
    return apk_name,label

if __name__ == '__main__':
    main()
