#!/bin/bash
#$ -cwd
#$ -pe openmp 4
#$ -q seal,free*,pub*
#$ -j y
#$ -t 1-24999
#$ -r y
#$ -ckpt restart

module load anaconda/2.7-4.3.1
SEEDFILE=/share/seal/joshug4/workspace/android-reflection-analysis/verified_androzoo_benign_apps.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/enec.sh $SEED
