#!/usr/bin/env python

import argparse
import os
import json

def main():
    parser = argparse.ArgumentParser(description='create a dataset from PAPI feature files')
    parser.add_argument('--file-list',help='file containing a list of PAPI feature files and their locations',required=True)
    parser.add_argument('--outfile',help='name of file to store dataset in',required=True)
    args = parser.parse_args()

    files = [line.rstrip('\n') for line in open(args.file_list)]

    print 'populating dataset from {}'.format(args.file_list)
    dataset=[]
    for f in files:
        fv=dict()
        apk_name,label=extract_metadata_from_path(f)
        fv['apk_name']=apk_name
        #print 'curr apk name: {}'.format(apk_name)
        for line in open(f):
            tokens = line.rstrip('\n').split(',')
            pkg = tokens[0]
            count = tokens[1]
            fv[pkg]=int(count)

        fv['label']=label
        dataset.append(fv)

    print 'storing to {}'.format(args.outfile)
    f = open(args.outfile,'w')
    f.write(json.dumps(dataset))

def extract_metadata_from_path(pathname):
    filename = os.path.basename(pathname)
    label = filename[:filename.find('_')]
    apk_name=filename.replace(label+'_','').replace('_apiusage.txt','')
    return apk_name,label

if __name__ == '__main__':
    main()
