#!/bin/bash
#$ -cwd
#$ -pe openmp 4
#$ -q seal
#$ -o /share/seal/joshug4/revealdroid_logs/
#$ -e /share/seal/joshug4/revealdroid_logs/
#$ -t 1-100
#$ -ckpt restart

module load anaconda/2-2.3.0
SEEDFILE=random_apk-paths_from_histogram.txt
SEED=$(awk "NR==$SGE_TASK_ID" $SEEDFILE)
/share/seal/joshug4/workspace/revealdroid/extract_prn_features.sh  $SEED

