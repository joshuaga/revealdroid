#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
import random
import os
import matplotlib

parser = argparse.ArgumentParser(description='create a histogram of for APK sizes (or any other similar data)')
parser.add_argument('-f',help='file with data to create histogram from')
parser.add_argument('--plot',dest='plot',action='store_true')
args = parser.parse_args()
print args

random.seed(1)

size_paths = dict()
sizes=[]
infile = open(args.f,'r')
for line in infile:
    tokens = line.split()
    size = int(tokens[0])
    apk_path = tokens[1]
    sizes.append(size)
    paths=set()
    if size in size_paths:
        paths = size_paths[size]
    paths.add(apk_path)
    size_paths[size]=paths

DEBUG=False
if DEBUG:
    for size,path_list in size_paths.items():
        print '{} : {}'.format(size,path_list)

num_bins=5
hist, bin_edges = np.histogram(sizes,bins=num_bins)
print 'values of histogram: {}'.format(hist)
print 'bin edges: {}'.format(bin_edges)

if args.plot:
    plt.figure(figsize=(8,4),dpi=300)
    #plt.hist(sizes, bins=bin_edges)
    plt.hist(sizes,log=True,bins=num_bins)
    #plt.title('Histogram with {} bins'.format(num_bins))
    #plt.ticklabel_format(style='sci', axis='x', scilimits=(0,60000000))
    ax = plt.gca()
    #ax.get_xaxis().get_major_formatter().set_useOffset(False)
    ax.get_xaxis().set_major_formatter(
            matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
    plt.xlabel('App Sizes in Bytes')
    plt.ylabel('Frequency on Log Scale')
    basename = os.path.splitext(os.path.basename(args.f))[0]
    plt.savefig('{}_histogram.pdf'.format(basename))

bin_indices=np.digitize(sizes,bin_edges,right=True)

size_bins=dict()
bin_sizes=dict()
for i,hist_bin in enumerate(bin_indices):
    size = sizes[i]
    size_bins[size]=hist_bin

    sizes_in_bin=set()
    if hist_bin in bin_sizes:
        sizes_in_bin = bin_sizes[hist_bin]
    sizes_in_bin.add(size)
    bin_sizes[hist_bin] = sizes_in_bin

sample_size=20
bin_samples=dict()
for hist_bin,sizes_in_bin in bin_sizes.items():
    print 'bin: {}'.format(hist_bin)
    print sizes_in_bin

    if sample_size > len(sizes_in_bin):
        print sizes_in_bin
    else:
        samples = random.sample(sizes_in_bin,sample_size)
        print samples
        bin_samples[hist_bin] = samples

size_count=1
sampled_paths=set()
for hist_bin,samples in bin_samples.items():
    print 'bin: {}'.format(hist_bin)
    for size in samples:
        print '{} : {}'.format(size_count,size)
        size_count=size_count+1
        for i,path in enumerate(size_paths[size]):
            if i>0:
                continue
            print '\t{}'.format(path)
            sampled_paths.add(path)

    
outfile = open('random_apk-paths_from_histogram.txt','w')

for path in sampled_paths:
    outfile.write('{}\n'.format(path.strip()))
outfile.close()

