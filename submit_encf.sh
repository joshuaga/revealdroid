#!/bin/bash
#$ -cwd
#disabled -pe openmp 2
#$ -q seal
#$ -j y
indir=$1
find $indir -type f -name "*.apk" -print | while read apk; do
    echo Submitting job for encf.sh $apk
    qsub encf.sh $apk
done
echo Submitted all encf.sh jobs
