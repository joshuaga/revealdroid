#!/usr/bin/env python

import argparse
import os
import numpy as np
import time

from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn import tree
from sklearn.externals import joblib

parser = argparse.ArgumentParser(description='Obtain extracted features and classify by the apk name')
parser.add_argument('apk',help='apk file location')
parser.add_argument('-c',metavar='C',default='r',help='select a classifier: k for nearest neighbor, d for decision tree (the default), s for SVM, l for linear SVC, r for logistic regression')
args = parser.parse_args()
print args.apk

basename = os.path.basename(args.apk)
print basename

prefix_name, ext = os.path.splitext(basename)
print prefix_name

def populate_fv(fv,in_f,IN_DIR):
    f = open(IN_DIR + in_f,'r')
    for line in f:
        k,v = line.strip().split(',')
        fv[k] = v
    return fv

papi_f = None
PAPI_DIR='data/apiusage/'
for f in os.listdir(PAPI_DIR):
    if f.endswith(prefix_name + '_apiusage.txt'):
        print 'Found matching papi file: {}'.format(f)
        papi_f=f
        break
fv = dict() # initialize feature vector when first used
fv = populate_fv(fv,papi_f,PAPI_DIR)
print fv

ref_f = None
REF_DIR='../android-reflection-analysis/data/'
for f in os.listdir(REF_DIR):
    if f.endswith(prefix_name + '_reflect.txt'):
        print 'Found matching ref file: {}'.format(f)
        ref_f = f
        break
fv = populate_fv(fv,ref_f,REF_DIR)
print fv

nec_f = None
NEC_DIR='data/native_external_calls/'
for f in os.listdir(NEC_DIR):
    if f.endswith(prefix_name + '_nec.txt'):
        print 'Found matching nec file: {}'.format(f)
        nec_f=f
        break
fv = populate_fv(fv,nec_f,NEC_DIR)
print fv


def perform_rep_analysis(args, fv):
    fnames_filename='../android-reflection-analysis/res/pvd_nrp_fn.csv'

    fnames=[line.rstrip('\n') for line in open(fnames_filename)]

    long_features=[]
    for fn in fnames:
        if fn in fv.keys():
            #print '{} is in this app\'s feature vector'.format(fn)
            long_features.append(fv[fn])
        else:
            #print '{} is NOT in this app\'s feature vector'.format(fn)
            long_features.append(0)

    X_test = np.array([long_features],dtype=float)

    clf_type = args.c
    clf_name = None
    if clf_type == 's':
        CLF_FILENAME='res/pvd_nrp_svc_classifier.pkl'
        clf_name = 'SVC'
    elif clf_type == 'l':
        CLF_FILENAME='res/pvd_nrp_linearsvc_classifier.pkl'
        clf_name = 'linearSVC'
    elif clf_type == 'r':
        CLF_FILENAME='res/pvd_nrp_logreg_classifier.pkl'
        clf_name = 'logistic regression'
    else:
        raise 'clf_type is unknown type of: {}'.format(clf_type)
    clf = None
    if os.path.isfile(CLF_FILENAME):
        print 'Loading classifier model from file {}...'.format(CLF_FILENAME)
        clf = joblib.load(CLF_FILENAME)
    else:
        train_x_filename='../android-reflection-analysis/res/pvd_nrp_x.csv'
        train_y_filename='../android-reflection-analysis/res/pvd_nrp_y.csv'
        print 'Loading training datasets from files...'
        start_time = time.time()
        X_train = np.loadtxt(train_x_filename,delimiter=',')
        y_train = np.loadtxt(train_y_filename,dtype=np.dtype(np.str_),delimiter=',')
        print 'Time to load training dataset: {}'.format(time.time()-start_time)

        print 'Building {} classifier...'.format(clf_name)
        start_time = time.time()
        if clf_type == 's':
            clf = SVC()
        elif clf_type == 'l':
            clf = LinearSVC(C=0.01,penalty="l1",dual=False)
        elif clf_type == 'r':
            clf = LogisticRegression(max_iter=1000)
        else:
            raise 'clf_type is unknown type of: {}'.format(clf_type)

        clf.fit(X_train,y_train)
        print 'Time to construct classifier: {}'.format(time.time()-start_time)
        print 'Storing classifier...'
        joblib.dump(clf,CLF_FILENAME)

    p_result = clf.predict(X_test)
    print 'Predicted reputation: {}'.format(p_result[0])

    c_result = clf.predict_proba(X_test)
    idx_label = clf.classes_.tolist().index(p_result[0])
    print 'confidence score: {}'.format(c_result[0][idx_label])

    if p_result[0] == 'Benign':
        return 0
    else:
        return 1

rep = perform_rep_analysis(args, fv)

def perform_fam_analysis(args, fv):
    fnames_filename='../android-reflection-analysis/res/90fam_nrp_fn.csv'

    fnames=[line.rstrip('\n') for line in open(fnames_filename)]

    long_features=[]
    for fn in fnames:
        if fn in fv.keys():
            #print '{} is in this app\'s feature vector'.format(fn)
            long_features.append(fv[fn])
        else:
            #print '{} is NOT in this app\'s feature vector'.format(fn)
            long_features.append(0)

    X_test = np.array([long_features],dtype=float)

    CLF_FILENAME='res/90fam_nrp_tree_classifier.pkl'

    if os.path.isfile(CLF_FILENAME):
        print 'Loading classifier model from file {}...'.format(CLF_FILENAME)
        clf = joblib.load(CLF_FILENAME)
    else:
        train_x_filename='../android-reflection-analysis/res/90fam_nrp_x.csv'
        train_y_filename='../android-reflection-analysis/res/90fam_nrp_y.csv'
        print 'Loading training datasets from files...'
        start_time = time.time()
        X_train = np.loadtxt(train_x_filename,delimiter=',')
        y_train = np.loadtxt(train_y_filename,dtype=np.dtype(np.str_),delimiter=',')
        print 'Time to load training dataset: {}'.format(time.time()-start_time)

        print 'Building decision-tree classifier...'
        start_time = time.time()
        clf = tree.DecisionTreeClassifier()
        clf.fit(X_train,y_train)
        print 'Time to construct classifier: {}'.format(time.time()-start_time)
        print 'Storing classifier...'
        joblib.dump(clf,CLF_FILENAME)

    p_result = clf.predict(X_test)
    pred_fam = p_result[0]
    print 'Predicted family: {}'.format(pred_fam)

    c_result = clf.predict_proba(X_test)
    idx_label = clf.classes_.tolist().index(p_result[0])
    print 'confidence score: {}'.format(c_result[0][idx_label])
    
    fam_desc_file = 'res/family_descriptions.txt'
    fam_descs = dict()
    for line in open(fam_desc_file):
        toks = line.split(':')
        fam = toks[0].replace('"','').strip()
        desc = toks[1].strip()
        fam_descs[fam] = desc

    if pred_fam in fam_descs:
        print 'family description: '
        print fam_descs[pred_fam]
    
if rep == 1:
    perform_fam_analysis(args,fv)
